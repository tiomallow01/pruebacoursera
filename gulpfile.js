import gulp from "gulp";
import imagemin from "gulp-imagemin";

gulp.task("imagemin", () => {
  gulp.src("images/*").pipe(imagemin()).pipe(gulp.dest("dist/images"));
});
