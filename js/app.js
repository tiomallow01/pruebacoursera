//Script para implementar y poder usar los tooltips //

$(function () {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-bs-toggle='popover']").popover();
  $(".carousel").carousel({
    interval: 2000,
  });
  $("#contacto").on("show.bs.modal", function (e) {
    console.log("Modal activado");

    $("#contacto").removeClass("btn-outline-succes");
    $("#contacto").addClass("btn-primary");
    $("#contacto").prop("disabled", true);
  });
  $("#contacto").on("shown.bs.modal", function (e) {
    console.log("Modal en curso");
  });
  $("#contacto").on("hide.bs.modal", function (e) {
    console.log("Modal ocultandose");
  });
  $("#contacto").on("hidden.bs.modal", function (e) {
    console.log("Modal Oculto");
    $("#contacto").prop("disabled", false);
  });
});
